<?php

class easycmsFlagsType extends eZDataType
{
  const DATA_TYPE_STRING = "easycmsflags";

  function easycmsflagstype()
  {
    $this->eZDataType( self::DATA_TYPE_STRING, "フラグ絞り込み" );
  }

  function validateObjectAttributeHTTPInput( $http, $base, $contentObjectAttribute )
  {
    return eZInputValidator::STATE_ACCEPTED;
  }


  function objectDisplayInformation( $objectAttribute, $mergeInfo = false )
  {
    $info = array( 'edit' => array( 'grouped_input' => true ),
      'collection' => array( 'grouped_input' => true ) );
    return eZDataType::objectDisplayInformation( $objectAttribute, $info );
  }

  function customObjectAttributeHTTPAction( $http, $action, $contentObjectAttribute, $parameters )
  {
    switch ( $action )
    {
    case "set_flag_target" :
    {
      if ( $http->hasPostVariable( 'BrowseActionName' ) and
        $http->postVariable( 'BrowseActionName' ) == ( 'AddTargetFlagDataType_' . $contentObjectAttribute->attribute( 'id' ) ) and
        $http->hasPostVariable( "SelectedObjectIDArray" ) )
      {
        if ( !$http->hasPostVariable( 'BrowseCancelButton' ) )
        {
          $selectedObjectIDArray = $http->postVariable( "SelectedObjectIDArray" );
          $objectID = $selectedObjectIDArray[0];
          $node = eZContentObjectTreeNode::fetchByContentObjectID( $objectID );
          $node_id = $node[0]->attribute('node_id');
          $contentObjectAttribute->setAttribute( "data_int", $node_id );
          $contentObjectAttribute->store();
        }
      }
    } break;

    case "browse_object" :
    {
      $module = $parameters['module'];
      $redirectionURI = $parameters['current-redirection-uri'];
      $ini = eZINI::instance( 'content.ini' );

      $browseParameters = array(  'action_name' => 'AddTargetFlagDataType_' . $contentObjectAttribute->attribute( 'id' ),
                                  'type' =>  'AddTargetFlagDataType',
                                  'browse_custom_action' => array( 
                                    'name' => 'CustomActionButton[' . $contentObjectAttribute->attribute( 'id' ) . '_set_flag_target]',
                                    'value' => $contentObjectAttribute->attribute( 'id' ) ),
                                  'persistent_data' => array( 'HasObjectInput' => 0 ),
                                  'from_page' => $redirectionURI );
      $browseTypeINIVariable = $ini->variable( 'ObjectRelationDataTypeSettings', 'ClassAttributeStartNode' );
      foreach( $browseTypeINIVariable as $value )
      {
        list( $classAttributeID, $type ) = explode( ';',$value );
        if ( $classAttributeID == $contentObjectAttribute->attribute( 'contentclassattribute_id' ) && strlen( $type ) > 0 )
        {
          $browseParameters['type'] = $type;
          break;
        }
      }

      $browseParameters['start_node'] = 2;
      eZContentBrowse::browse( $browseParameters, $module );
    } break;

    default :
    {
      eZDebug::writeError( "Unknown custom HTTP action: " . $action, "eZObjectRelationType" );
    } break;
    }
  }


  function format_date( $date ){
    $date = str_replace( 'yyyy', '%Y', $date );
    $date = str_replace( 'yy', '%y', $date );
    $date = str_replace( 'mm', '%m', $date );
    $date = str_replace( 'dd', '%d', $date );
    $date = str_replace( 'hh', '%H', $date );
    $date = str_replace( 'nn', '%i', $date );
    return $date;
  }

  function objectAttributeContent( $contentObjectAttribute )
  {
    $content = unserialize($contentObjectAttribute->attribute('data_text'));
    $content['parent_node_id'] = $contentObjectAttribute->attribute( "data_int" );
    return $content;
  }


  function fetchObjectAttributeHTTPInput( $http, $base, $contentObjectAttribute )
  {
    if ( 
      $http->hasPostVariable( $base . '_flag_viewmode_' . $contentObjectAttribute->attribute( 'id' ) ) 
    )
    {
      $content = $contentObjectAttribute->attribute( "content" ); 
      if ($http->hasPostVariable( $base . '_flag_list_' . $contentObjectAttribute->attribute( 'id' ) ) ){
        $flag_list = array_keys( $http->postVariable( $base . '_flag_list_' . $contentObjectAttribute->attribute( "id" ) ) );
      } else {
        $flag_list = array();
      }
      $content['flag_list'] = $flag_list;
      $date_format = $http->postVariable( $base . '_flag_date_format_' . $contentObjectAttribute->attribute( "id" ) );
      $content['date_format'] = $date_format;
      $content['date_format_ez'] = $this->format_date( $date_format );
      $items_per_page = $http->postVariable( $base . '_flag_items_per_page_' . $contentObjectAttribute->attribute( "id" ) );
      $content['items_per_page'] = $items_per_page;
      $viewmode = $http->postVariable( $base . '_flag_viewmode_' . $contentObjectAttribute->attribute( "id" ) );
      $content['viewmode'] = $viewmode;
      $sort_order = $http->postVariable( $base . '_flag_sort_order_' . $contentObjectAttribute->attribute( "id" ) );
      $content['sort_order'] = $sort_order;
      $sort_field = $http->postVariable( $base . '_flag_sort_field_' . $contentObjectAttribute->attribute( "id" ) );
      $content['sort_field'] = $sort_field;
      $contentObjectAttribute->setAttribute( "data_text", serialize( $content ) );
    }
    return true;
  }

  function initializeObjectAttribute( $objectAttribute, $currentVersion, $originalContentObjectAttribute )
  {
    if ( $currentVersion != false )
    {
      $dataText = $originalContentObjectAttribute->attribute( "data_text" );
      $objectAttribute->setAttribute( "data_text", $dataText );
    } else {
      $content = array( 'flag_list' => array(), 
                        'viewmode' => null, 
                        'items_per_page' => null, 
                        'sort_field' => null, 
                        'sort_order' => null, 
                        'date_format' => null, 
                        'parent_node_id' => null );
      $objectAttribute->setAttribute( "data_text", serialize($content) );
    }
  }

  function metaData( $contentObjectAttribute )
  {
    return "";
  }

  function title( $contentObjectAttribute, $name = null )
  {
    return $contentObjectAttribute->attribute( "data_text" );
  }

  function isIndexable()
  {
    return true;
  }
}
eZDataType::register( easycmsFlagsType::DATA_TYPE_STRING, "easycmsFlagsType" );
?>


