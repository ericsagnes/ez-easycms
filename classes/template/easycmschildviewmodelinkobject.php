<?php

class easycmsChildViewModeLinkObject extends eZPersistentObject
{
    /**
     * Constructor
     *
     */
    function __construct( $row )
    {
        parent::__construct( $row );
    }


    static function definition()
    {
        return array( 'fields'              => array( 'node_id'                   => array( 'name'     => 'node_id',
                                                                                            'datatype' => 'integer',
                                                                                            'default'  => 0,
                                                                                            'required' => true ),
                                                      'view'                      => array( 'name'     => 'view',
                                                                                            'datatype' => 'string',
                                                                                            'default'  => '',
                                                                                            'required' => false ) ),
                      'keys'                => array( 'node_id' ),
                      'class_name'          => 'easycmsChildViewModeLinkObject',
                      'sort'                => array( 'priority' => 'asc' ),
                      'name'                => 'easycms_childtemplate_link' );
    }

    static function implicit_viewmode( $node_id )
    {
      if( $view_mode = eZPersistentObject::fetchObject( self::definition(), null, array( 'node_id' => $node_id ) ) )
      {
        return $view_mode->attribute( 'view' );
      } else {
        return false;
      }
    }

    static function viewmode( $node_id )
    {
      $viewmodes_list_settings = eZINI::instance('easycms.ini')->variable('Templates', 'viewmodes');
      $base_node = eZContentObjectTreeNode::fetch($node_id);
      $node = $base_node;
      $implicit_viewmode = easycmsChildViewModeLinkObject::implicit_viewmode( $node->attribute('node_id'));
      $viewmode = $implicit_viewmode;
      $herited_type = $implicit_viewmode == 'inherit' ? true : false;
      if( $implicit_viewmode == false || $implicit_viewmode == 'inherit' ){
        while($node->attribute('node_id') != "1")
        {
          $current_viewmode = easycmsChildViewModeLinkObject::implicit_viewmode( $node->attribute('node_id') );
          if( $node->attribute('node_id') != $node_id ){
            if( $current_viewmode != false and $current_viewmode != "inherit" )
            {
              $herited_type = 'child';
              $view = $current_viewmode;
              break;
            }
          }
          $current_viewmode = easycmsViewModeLinkObject::implicit_viewmode( $node->attribute('node_id') );
          if( $current_viewmode != false and $current_viewmode != "inherit" )
          {
            $herited_type = 'node';
            $viewmode = $current_viewmode;
            break;
          }
          $node = $node->attribute('parent');
        }
      }
      if($node->attribute('node_id') == "1"){
        $viewmode = 'default';
        $herited_type = 'default';
      }
      return array( 
          'viewmode' => $viewmode, 
          'viewmode_name' => $viewmodes_list_settings[$viewmode], 
          'implicit_viewmode' => $implicit_viewmode, 
          'herited' => ($herited_type != false),
          'herited_from' => $node,
          'herited_type' => $herited_type 
      );
    }

}

?>
