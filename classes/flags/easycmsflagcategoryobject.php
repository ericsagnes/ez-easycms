<?php

/**
 * FlagObject class inherits eZPersistentObject class
 * to be able to access eztags database table through API
 *
 */
class easycmsFlagCategoryObject extends eZPersistentObject
{

  /**
   * Returns the definition array for eZTagsObject
   *
   * @return array
   */
  static function definition()
  {
    return array( 'fields'  => array( 
                                      'id'               => array( 'name'     => 'id',
                                                                   'datatype'  => 'integer',
                                                                   'default'  => 0,
                                                                   'required' => true ),
                                      'name'             => array( 'name'     => 'name',
                                                                   'datatype' => 'string',
                                                                   'default'  => '',
                                                                   'required' => false ),
                                      'contentobject_id' => array( 'name'     => 'contentobject_id',
                                                                   'datatype' => 'string',
                                                                   'default'  => '',
                                                                   'required' => false ),
                                      'system_category'  => array( 'name'     => 'system_category',
                                                                   'datatype' => 'int',
                                                                   'default'  => 0,
                                                                   'required' => false ) ),
                  'function_attributes' => array( 'flags'                => 'getFlags',
                                                  'assigned_flags_id'    => 'getAssignedFlagsID'),
                                                  'keys'                => array( 'id' ),
                                                  'increment_key'       => 'id',
                                                  'class_name'          => 'easycmsFlagCategoryObject',
                                                  'sort'                => array( 'id' => 'asc' ),
                  'name'                => 'easycms_flags_category' );
  }


  static function fetchCategoryList()
  {
    return eZPersistentObject::fetchObjectList( self::definition(), null, null, null );
  }

  static function fetchObjectCategoryFlagList( $object_id, $category )
  {
      $flag_category = eZPersistentObject::fetchObject( self::definition(), null, array( 'contentobject_id' => $object_id, 'name' => $category ) );
      if ( $flag_category ){
          $return = $flag_category->attribute('flags');
      } else {
          $return = false;
      }
      return $return;
  }

  static function deleteObject( $contentobject_id )
  {
    $categories = eZPersistentObject::fetchObjectList( self::definition(), null, array( 'contentobject_id' => $contentobject_id ) );
    foreach ( $categories as $category )
    {
      easycmsFlagCategoryLinkObject::deleteObject( $category->attribute('id') );
      $category->remove();
    }
  }

  static function fetchNodeCategoriesList( $node_id )
  {
    $object_id = eZContentObject::fetchByNodeID( $node_id )->attribute('id');
    $categories = eZPersistentObject::fetchObjectList( self::definition(), null, array( 'contentobject_id' => (int) $object_id ), null, null, false );
    return $categories;
  }

  static function fetchContentFlagData( $node_id )
  {
    $result = easycmsFlagCategoryObject::fetchContainerFlagData( $node_id );
    return $result; 
  }

  static function fetchContainerFlagData( $node_id )
  {
    $result = array();
    $object_id = eZContentObject::fetchByNodeID( $node_id )->attribute('id');
    $categories = eZPersistentObject::fetchObjectList( self::definition(), null, array( 'contentobject_id' => (int) $object_id ), null, null, false );
    $all_flags = eZPersistentObject::fetchObjectList( easycmsFlagObject::definition(), null, array( 'contentobject_id' => (int) $object_id ), null, null, true );
    $no_category_flags = $all_flags;

    foreach( $categories as $category ){
      $category_flags = eZPersistentObject::fetchObjectList( 
        easycmsFlagCategoryLinkObject::definition(), null, array( 'flag_category_id' => (int) $category['id'] ), null, null, true );
      $category_flag_array = array();
      foreach( $category_flags as $category_flag ){
        $category_flag_array[] = array_pop( array_filter( $all_flags, function($item) use($category_flag){  
          return ( $item->attribute('id') == $category_flag->attribute('flag_id') );
        } ) );
        $no_category_flags = array_filter( $no_category_flags, function($item) use($category_flag){  
          return ( $item->attribute('id') != $category_flag->attribute('flag_id') );
        } );
      }
      usort($category_flag_array, function($a,$b){ return $b->attribute('priority') - $a->attribute('priority'); });
      usort($no_category_flags, function($a,$b){ return $b->attribute('priority') - $a->attribute('priority'); });
      $result[] = array( 'name' => $category['name'], 'id' => $category['id'], 'system_category' => $category['system_category'], 'flags' => $category_flag_array );
    }
    $result[] = array( 'name' => 'カテゴリなし', 'id' => 'empty', 'flags' => $no_category_flags, 'system_category' => 0 );
    return $result;  
  }

  function getFlags( )
  {
    $id = (int) $this->attribute( 'id' );
    return easycmsFlagCategoryLinkObject::fetchFlagList( $id );
  }

  function getAssignedFlagsID(  )
  {
    $id = (int) $this->attribute( 'id' );
    return easycmsFlagCategoryLinkObject::fetchFlagList( $id, false );
  }
}

?>

