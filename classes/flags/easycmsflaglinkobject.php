<?php

/**
 * eZTagsAttributeLinkObject class inherits eZPersistentObject class
 * to be able to access eztags_attribute_link database table through API
 *
 */
class easycmsFlagLinkObject extends eZPersistentObject
{

    /**
     * Returns the definition array for eZTagsAttributeLinkObject
     *
     * @return array
     */
    static function definition()
    {
        return array( 'fields'        => array( 'contentobject_id'        => array( 'name'     => 'contentobject_id',
                                                                                    'datatype' => 'integer',
                                                                                    'default'  => 0,
                                                                                    'required' => true ),
                                                'flag_id'                 => array( 'name'     => 'flag_id',
                                                                                    'datatype' => 'integer',
                                                                                    'default'  => 0,
                                                                                    'required' => true )),
                      'class_name'    => 'easycmsflagLinkObject',
                      'keys'          => array( 'contentobject_id', 'flag_id' ),
                      'sort'          => array( 'flag_id' => 'asc' ),
                      'name'          => 'easycms_flags_link' );
    }

    static function deleteObject( $contentobject_id )
    {
        $flag_links = eZPersistentObject::fetchObjectList( self::definition(), null, array( 'contentobject_id' => $contentobject_id ) );
        foreach ( $flag_links as $link )
        {
            $link->remove();
        }
    }

    static function fetchFlagList( $contentobject_id )
    {
        $flag_links = eZPersistentObject::fetchObjectList( self::definition(), null, array( 'contentobject_id' => $contentobject_id ) );
        if ( is_array( $flag_links ) && !empty( $flag_links ) )
        {
            $flags = array();
            foreach ( $flag_links as $flag )
            {
                $flags = array_merge( $flags, eZPersistentObject::fetchObjectList( easycmsFlagObject::definition(), null, array( 'id' => $flag->attribute('flag_id') ) ) );
            }
            return $flags;
        }
    }

    static function fetchObjectIDList( $flag_id )
    {
        $result = eZPersistentObject::fetchObjectList( self::definition(), null, array( 'flag_id' => $flag_id ) );
        if ( is_array( $result ) && !empty( $result ) )
        {
            $objectIDArray = array();
            foreach ( $result as $r )
            {
                $id = (int) $r->attribute( 'contentobject_id' );
                array_push( $objectIDArray, $id );
            }
            return $objectIDArray;
        }
        return array();
    }

    static function exists( $contentobject_id, $flag_id )
    {
        $result = eZPersistentObject::fetchObject( self::definition(), null, array( 'flag_id' => $flag_id, 'contentobject_id' => $contentobject_id ) );
        if ( $result instanceof easycmsFlagLinkObject )
        {
            return $result;
        }
        return false;
    }

    static function fetchObjectList( $flag_id )
    {
        $result = eZPersistentObject::fetchObjectList( self::definition(), null, array( 'flag_id' => $flag_id ) );
        if ( is_array( $result ) && !empty( $result ) )
        {
            $objectIDArray = array();
            foreach ( $result as $r )
            {
                $id = (int) $r->attribute( 'contentobject_id' );
                array_push( $objectIDArray, $id );
            }

            return eZContentObject::fetchIDArray( $objectIDArray );
        }
        return array();
    }
}

?>
