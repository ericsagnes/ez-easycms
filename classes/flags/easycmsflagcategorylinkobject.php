<?php

/**
 * eZTagsAttributeLinkObject class inherits eZPersistentObject class
 * to be able to access eztags_attribute_link database table through API
 *
 */
class easycmsFlagCategoryLinkObject extends eZPersistentObject
{

    /**
     * Returns the definition array for eZTagsAttributeLinkObject
     *
     * @return array
     */
    static function definition()
    {
        return array( 'fields'        => array( 'flag_id'        => array( 'name'=> 'flag_id',
                                                                                    'datatype' => 'integer',
                                                                                    'default'  => 0,
                                                                                    'required' => true ),
                                                'flag_category_id'=> array( 'name'     => 'flag_category_id',
                                                                                    'datatype' => 'integer',
                                                                                    'default'  => 0,
                                                                                    'required' => true )),
                      'class_name'    => 'easycmsFlagCategoryLinkObject',
                      'keys'          => array( 'flag_id', 'flag_category_id' ),
                      'sort'          => array( 'flag_category_id' => 'asc' ),
                      'name'          => 'easycms_flags_category_link' );
    }

    static function deleteObject( $flag_category_id )
    {
      $categories = eZPersistentObject::fetchObjectList( self::definition(), null, array( 'flag_category_id' => $contentobject_id ) );
      foreach ( $categories as $category )
      {
        $category->remove();
      }
    }

    static function fetchCategoryList( $flag_id )
    {
      # working
      $result =  eZPersistentObject::fetchObjectList( self::definition(), array('flag_category_id'), array( 'flag_id' => $flag_id ), null, null, false );
      if ( is_array( $result ) && !empty( $result ) )
      {
        $objectIDArray = array();
        foreach ( $result as $r )
        {
          $id = (int) $r['flag_category_id'];
          $objectIDArray = array_merge( $objectIDArray, eZPersistentObject::fetchObjectList( easycmsFlagCategoryObject::definition(), null, array( 'id' => $id ) ) );
        }
        return $objectIDArray;
      }
      return false;
    }
    static function fetchAllFlagsID(  )
    {
      $result = eZPersistentObject::fetchObjectList( self::definition(), null, null, null, null, false );
      if ( is_array( $result ) && !empty( $result ) )
      {
        $flagIDArray = array();
        foreach ( $result as $r )
        {
          $id = (int) $r['flag_id'];
          array_push( $flagIDArray, $id );
        }
        return array_unique($flagIDArray);
      }
      return array();
    }

    static function fetchFlagList( $flag_category_id, $as_object = true )
    {
      $result =  eZPersistentObject::fetchObjectList( self::definition(), array('flag_id'), array( 'flag_category_id' => $flag_category_id ), null, null, false );
      if ( is_array( $result ) && !empty( $result ) )
      {
        $objectIDArray = array();
        foreach ( $result as $r )
        {
          $id = (int) $r['flag_id'];
          if ($as_object==true)
            $objectIDArray = array_merge( $objectIDArray, eZPersistentObject::fetchObjectList( easycmsFlagObject::definition(), null, array( 'id' => $id ) ) );
          else
            array_push( $objectIDArray, $id );

        }
        usort($objectIDArray, function($a,$b){ return $a->attribute('priority') - $b->attribute('priority'); });
        return $objectIDArray;
      }
      return false;
    }
}

?>

