<?php

/**
 * eZTagsAttributeFilter class implements TagsAttributeFilter extended attribute
 *
 */
class easycmsFlagsAttributeFilter
{
    /**
     * Creates and returns SQL parts used in fetch functions
     *
     * @return array
     */
    function createSqlParts( $params )
    {
        $returnArray = array( 'tables' => '', 'joins'  => '', 'columns' => '', 'group_by' => '' );

        if ( isset( $params['flag_id'] ) )
        {
            if ( is_array( $params['flag_id'] ) )
            {
                $flagIDsArray = $params['flag_id'];
            }
            else if ( (int) $params['flag_id'] > 0 )
            {
                $flagIDsArray = array( (int) $params['flag_id'] );
            }
            else
            {
                return $returnArray;
            }

            $returnArray['tables'] = ", easycms_flags_link i1 ";

            $db = eZDB::instance();
            $dbString = $db->generateSQLINStatement( $flagIDsArray, 'i1.flag_id', false, true, 'int' );

            $returnArray['joins'] = " $dbString AND i1.contentobject_id = ezcontentobject.id AND ";
            $returnArray['group_by'] = " group by i1.contentobject_id having count(distinct i1.flag_id) = ". count($flagIDsArray) ." ";
        }
        else if ( isset( $params['flag_name'] ) )
        {
            if ( is_array( $params['flag_name'] ) )
            {
                $flagNamesArray = $params['flag_name'];
            }
            else if ( is_string( $params['flag_name'] ) )
            {
                $flagNamesArray = array( $params['flag_name'] );
            }
            else
            {
                return $returnArray;
            }

            $flagIDsArray = array();
            foreach ( $flagNamesArray as $name )
            {
                $flagIDsArray[] = easycmsFlagObject::getID( $name, $params['object_id'] );
            }

            $returnArray['tables'] = ", easycms_flags_link i1 ";

            $db = eZDB::instance();
            $dbString = $db->generateSQLINStatement( $flagIDsArray, 'i1.flag_id', false, true, 'int' );

            $returnArray['joins'] = " $dbString AND i1.contentobject_id = ezcontentobject.id AND ";
        }
        else if ( isset( $params['object_id'] ) )
        {

            $flagArray = easycmsFlagObject::fetchObjectFlagList( $params['object_id'] );
            $flagIDsArray = array();
            foreach ( $flagArray as $flag )
            {
                $flagIDsArray[] = $flag->attribute('id');
            }

            $returnArray['tables'] = ", easycms_flags_link i1 ";

            $db = eZDB::instance();
            $dbString = $db->generateSQLINStatement( $flagIDsArray, 'i1.flag_id', false, true, 'int' );

            $returnArray['joins'] = " $dbString AND i1.contentobject_id = ezcontentobject.id AND ";
            $returnArray['group_by'] = " group by i1.contentobject_id having count(distinct i1.flag_id) = ". count($flagIDsArray) ." ";
        }

        return $returnArray;
    }
}

?>

