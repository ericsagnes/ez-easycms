<?php

/**
 * FlagObject class inherits eZPersistentObject class
 * to be able to access eztags database table through API
 *
 */
class easycmsFlagObject extends eZPersistentObject
{
    /**
     * Constructor
     *
     */
    function __construct( $row )
    {
        parent::__construct( $row );
    }


    static function definition()
    {
        return array( 'fields'              => array( 'id'                  => array( 'name'     => 'id',
                                                                                      'datatype' => 'integer',
                                                                                      'default'  => 0,
                                                                                      'required' => true ),
                                                      'name'                => array( 'name'     => 'name',
                                                                                      'datatype' => 'string',
                                                                                      'default'  => '',
                                                                                      'required' => false ),
                                                      'priority'            => array( 'name'     => 'priority',
                                                                                      'datatype' => 'integer',
                                                                                      'default'  => 0,
                                                                                      'required' => false ),
                                                      'contentobject_id'    => array( 'name'     => 'contentobject_id',
                                                                                      'datatype' => 'string',
                                                                                      'default'  => '',
                                                                                      'required' => false ) ),
                      'function_attributes' => array( 'category'                  => 'getCategory',
                                                      'assigned_objects'          => 'getAssignedObjects',
                                                      'category_id'               => 'category_id',
                                                      'system_flag'               => 'systemFlag',
                                                      'assigned_objects_id'       => 'getAssignedObjectsID'),
                      'keys'                => array( 'id' ),
                      'increment_key'       => 'id',
                      'class_name'          => 'easycmsFlagObject',
                      'sort'                => array( 'priority' => 'asc' ),
                      'name'                => 'easycms_flags' );
    }

    static function deleteObject( $contentobject_id )
    {
        $flags = eZPersistentObject::fetchObjectList( self::definition(), null, array( 'contentobject_id' => $contentobject_id ) );
        foreach ( $flags as $flag )
        {
            $flag->remove();
        }
    }

    static function uncategorizedFlagList(  )
    {
        $categorized_flags = easycmsFlagCategoryLinkObject::fetchAllFlagsID();
        $all_flags = easycmsFlagObject::fetchAllFlagsIDList();
        $uncategorized_flags = array_diff($all_flags, $categorized_flags);
        if ( is_array( $uncategorized_flags ) && !empty( $uncategorized_flags ) )
        {
            $objectIDArray = array();
            foreach ( $uncategorized_flags as $id )
            {
                $objectIDArray = array_merge( $objectIDArray, eZPersistentObject::fetchObjectList( easycmsFlagObject::definition(), null, array( 'id' => $id ) ) );

            }
            return $objectIDArray;
        }
    }

    static function fetchAllFlagsIDList()
    {
        $result = eZPersistentObject::fetchObjectList( self::definition(), array('id'), null, null, null, false );
        if ( is_array( $result ) && !empty( $result ) )
        {
            $flagIDArray = array();
            foreach ( $result as $r )
            {
                $id = (int) $r['id'];
                array_push( $flagIDArray, $id );
            }
            return array_unique($flagIDArray);
        }
        return false;
    }

    static function fetchFlag( $flag_id )
    {
        $flag = eZPersistentObject::fetchObject( self::definition(), null, array( 'id' => $flag_id ) );
        return $flag;
    }


    static function fetchObjectFlagList( $object_id )
    {
      $node = eZContentObjectTreeNode::fetchByContentObjectID( $object_id );
      if( is_null($node) || ! $node ){
        return false;
      }
      $node = $node[0];
      $class_name = $node->attribute('class_identifier');
      $container_classes = eZINI::instance('easycms.ini')->variable('Content', 'ContainerClass');
      if(in_array($class_name,$container_classes)){
        $flags = eZPersistentObject::fetchObjectList( self::definition(), null, array( 'contentobject_id' => $object_id ) );
      }else{
        $flags = easycmsFlagLinkObject::fetchFlagList( $object_id );
      }
      $flags = array_filter( $flags, function($item) {  
          return ( ! $item->attribute('system_flag') );
      } );
      usort($flags, function($a,$b){ return $a->attribute('priority') - $b->attribute('priority'); });
      return $flags;
    }

    static function fetchObjectCategoryFlagList( $object_id, $category_id )
    {
      $flags = easycmsFlagObject::fetchObjectFlagList( $object_id );
      $flags = array_filter( $flags, function($item) use ($category_id) {  
          return ( $item->category_id() == $category_id );
      } );
      return array_values($flags);
    }

    static function fetchFlagList( )
    {
        return eZPersistentObject::fetchObjectList( self::definition(), null, null, null );
    }

    function systemFlag( )
    {
        $id = (int) $this->attribute( 'id' );
        $category_link = eZPersistentObject::fetchObjectList( easycmsFlagCategoryLinkObject::definition(), null, array( 'flag_id' => $id ) );
        if ( $category_link ){
            $category_id = $category_link[0]->attribute( 'flag_category_id' );
            if ($category_id){
                $category = eZPersistentObject::fetchObjectList( easycmsFlagCategoryObject::definition(), null, array( 'id' => $category_id ) );
                if ( $category[0]->attribute('system_category') == 1 ){
                    return true;
                }
            }
        }
        return false;
    }

    function getCategory()
    {
        $id = (int) $this->attribute( 'id' );
        return eZPersistentObject::fetchObject( easycmsFlagCategoryLinkObject::definition(), null, array( 'flag_id' => $id ) );
    }

    function category_id()
    {   
        $id = (int) $this->attribute( 'id' );
        $categories = easycmsFlagCategoryLinkObject::fetchCategoryList( $id );
        if(is_a($categories[0], 'easycmsFlagCategoryObject')){
            return (int) $categories[0]->attribute('id');
        }else{
            return 999999;
        }   
    }

    function getAssignedObjects(  )
    {
        $id = (int) $this->attribute( 'id' );
        return easycmsFlagLinkObject::fetchObjectList( $id );
    }

    function getAssignedObjectsID(  )
    {
        $id = (int) $this->attribute( 'id' );
        return easycmsFlagLinkObject::fetchObjectIDList( $id );
    }

    static function getID( $flag_name, $object_id )
    {
        $flags = eZPersistentObject::fetchObjectList( self::definition(), null, array( 'name' => $flag_name ) );
        foreach ( $flags as $flag ) 
        {
            $categories = $flag->getCategories();
            if( $categories[0]->attribute('contentobject_id') == $object_id ){
                return $flag->attribute('id');
            }
        }
        return false;
    }


}

?>
