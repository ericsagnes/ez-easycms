<?php /* #?ini charset="utf-8"?

[WindowControlsSettings]
AdditionalTabs[]=flags
AdditionalTabs[]=templates

[AdditionalTab_flags]
Title=フラグ管理
Description=フラグの管理
NavigationPartName=ezcontentnavigationpart
HeaderTemplate=user/flags_header.tpl
Template=user/flags.tpl

[AdditionalTab_templates]
Title=テンプレート
Description=テンプレートの管理
NavigationPartName=ezcontentnavigationpart
HeaderTemplate=user/templates_header.tpl
Template=user/templates.tpl

*/ ?>
