<?php /*

[Content]
ContainerClass[]
ContainerClass[]=folder

[Flags]
viewmodes[]
viewmodes[line]=ライン
sort_fields[]
sort_fields[name]=コンテンツ名
sort_fields[priority]=優先度
sort_fields[published]=作成日時
sort_fields[modified]=変更日時

[Templates]
viewmodes[]
viewmodes[inherit]=継承する
viewmodes[default]=デフォルト
layouts[]
layouts[inherit]=継承する
layouts[default]=デフォルト

 */ ?>
