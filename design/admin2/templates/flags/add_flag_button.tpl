{def $categories = fetch('flags','categories', hash(
    'node_id', $node.node_id
))}

    {run-once}
    {ezscript_require( array( 'ezjsc::yui3', 'ezjsc::yui3io', 'ezmodalwindow.js', 'ezajaxuploader.js' ) )}
    <div id="add-flag-modal-window" class="modal-window" style="display:none;">
        <h2>フラグの追加<a href="#" class="window-close">{'Close'|i18n( 'design/admin/pagelayout' )}</a><span></span></h2>
        <div class="blank-content"></div>
        <div>
        <form action={'flags/addflag'|ezurl} method="post">
        <input type="hidden" value="{$node.url_alias}" name="RedirectRelativeURI">
        <input type="hidden" value="{$node.contentobject_id}" name="FlagObjectID" />
        <fieldset>
        <p>フラグ名:<input type="text" name="FlagName"></p>
        <p>フラグの優先度:<input type="text" name="FlagPriority"></p>
        <p>フラグのカテゴリ:<select name="Category">
        <option value="empty">なし</option>
{foreach $categories as $category}
        <option value="{$category.id}">{$category.name}</option>
{/foreach}
        </select>
        </p>
        <p><input type="submit" value="フラグの追加" class="button" name="AddFlagButton"></p>
        </select></p>
        </fieldset>
        </form>
        </div>
    </div>
    <script type="text/javascript">
    {literal}
    (function () {
        YUI(YUI3_config).use('ezmodalwindow', function (Y) {

            var windowConf = {
                window: '#add-flag-modal-window',
                content: '.blank-content',
                centered: true,
                width: 400
            };

            Y.on('domready', function() {
                var win = new Y.eZ.ModalWindow(windowConf, Y);
                var button = Y.one("#add-flag-button")
                button.on('click', function (e) {
                                          win.open();
                                          e.halt();
                                       });
                
                button.removeClass('hide');

            });
        });

    })();
    {/literal}
    </script>
    {/run-once}


