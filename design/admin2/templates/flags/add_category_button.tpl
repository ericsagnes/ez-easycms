    {run-once}
    {ezscript_require( array( 'ezjsc::yui3', 'ezjsc::yui3io', 'ezmodalwindow.js', 'ezajaxuploader.js' ) )}
    <div id="add-category-modal-window" class="modal-window" style="display:none;">
        <h2>カテゴリの追加<a href="#" class="window-close">{'Close'|i18n( 'design/admin/pagelayout' )}</a><span></span></h2>
        <div class="blank-content"></div>
        <div>
        <form action={'flags/addcategory'|ezurl} method="post">
        <input type="hidden" value="{$node.url_alias}" name="RedirectRelativeURI" />
        <input type="hidden" value="{$node.contentobject_id}" name="CategoryObjectID" />
        <fieldset>
        <p>カテゴリ名:<input type="text" name="CategoryName"></p>
        <p>システムカテゴリ:<input type="checkbox" name="CategorySystem"></p>
        <p><input type="submit" value="カテゴリの追加" class="button" name="AddCategoryButton"></p>
        </fieldset>
        </form>
        </div>
    </div>
    <script type="text/javascript">
    {literal}
    (function () {
        YUI(YUI3_config).use('ezmodalwindow', function (Y) {

            var windowConf = {
                window: '#add-category-modal-window',
                content: '.blank-content',
                centered: true,
                width: 400
            };

            Y.on('domready', function() {
                var win = new Y.eZ.ModalWindow(windowConf, Y);
                var button = Y.one("#add-category-button")
                button.on('click', function (e) {
                                          win.open();
                                          e.halt();
                                       });
                
                button.removeClass('hide');

            });
        });

    })();
    {/literal}
    </script>
    {/run-once}
