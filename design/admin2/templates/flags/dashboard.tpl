{ezcss_require('flags.css')}
{def $categories = fetch('flags','categories')}

{foreach $categories as $category}
<div class="block">
<table class="list" cellspacing="0">
<tr>
<th width="100%">{$category.name}</th>
<th>
<input type="submit" value="削除" class="button delete-category-button" />
<input type="hidden" name="category_id" value="{$category.id}" />
<input type="hidden" name="category_name" value="{$category.name}" />
</th>
<th>
<input type="submit" value="編集" class="button update-category-button" />
<input type="hidden" name="category_id" value="{$category.id}" />
<input type="hidden" name="category_name" value="{$category.name}" />
</th>
</tr>
{foreach $category.flags as $flag sequence array('bg-light','bg-dark') as $style}
<tr class="{$style}">
<td>{$flag.name}</td>
<td>
<input type="submit" value="削除" class="button delete-flag-button hide" />
<input type="hidden" name="flag_id" value="{$flag.id}" />
<input type="hidden" name="flag_name" value="{$flag.name}" />
</td>
<td>
<input type="submit" value="編集" class="button update-flag-button hide" />
<input type="hidden" name="category" value="{$category.id}" />
<input type="hidden" name="flag_name" value="{$flag.name}" />
<input type="hidden" name="flag_id" value="{$flag.id}" />
</td>
</tr>
{/foreach}
</table>
</div>
{/foreach}

{def $uncategorized_flags = fetch('flags', 'uncategorized_flags')}
<div class="block">
<table class="list" cellspacing="0">
<tr>
<th width="100%" colspan="3">カテゴリされてないフラグ</th>
</tr>
{foreach $uncategorized_flags as $flag sequence array('bg-light','bg-dark') as $style}
<tr class="{$style}">
<td width="100%">{$flag.name}</td>
<td><input type="submit" value="削除" class="button delete-flag-button hide" />
<input type="hidden" name="flag_id" value="{$flag.id}" />
<input type="hidden" name="flag_name" value="{$flag.name}" />
</td>
<td><input type="submit" value="編集" class="button update-flag-button hide" />
<input type="hidden" name="category" value="empty" />
<input type="hidden" name="flag_name" value="{$flag.name}" />
<input type="hidden" name="flag_id" value="{$flag.id}" />
</td>
</tr>
{/foreach}
</table>
</div>

<div>
<input type="submit" value="{'カテゴリの追加'|i18n( 'design/admin/node/view/full' )}" id="add-category-button" class="button hide" />
<input type="submit" value="フラグの追加" class="button hide" id="add-flag-button" title="新規フラグを追加する" />
</div>

{include uri='design:flags/add_flag_button.tpl' url='flags/dashboard'}
{include uri='design:flags/add_category_button.tpl' url='flags/dashboard'}
{include uri='design:flags/update_flag_button.tpl' url='flags/dashboard'}
{include uri='design:flags/update_category_button.tpl' url='flags/dashboard'}
{include uri='design:flags/delete_flag_button.tpl' url='flags/dashboard'}
{include uri='design:flags/delete_category_button.tpl' url='flags/dashboard'}
