{def $path_node = ""}

[ {foreach $node.path_array as $path_item_id offset 1 max $id}
{set $path_node = fetch('content', 'node', hash('node_id', $path_item_id))}
{if eq($path_node.node_id,$node.node_id)}
{$path_node.name}
{else}
<a href={$path_node.url_alias|ezurl()}>{$path_node.name}</a>
{/if}
{delimiter} &gt; {/delimiter}
{/foreach} ]
