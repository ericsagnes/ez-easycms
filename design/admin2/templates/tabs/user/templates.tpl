{def $viewmode = fetch('template', 'viewmode_list', hash('node_id', $node.node_id))}

<div class="block">
<table class="list" cellspacing="0">
<thead>
<tr>
<tr><th width="100%">ノードテンプレート</th>
</tr>
</thead>
<tbody>
<tr><td>
<form action={'template/update'|ezurl} method="post">
<input type="hidden" name="Type" value="node_template">
<input type="hidden" name="NodeID" value="{$node.node_id}">
<input type="hidden" value="{$node.url_alias}" name="RedirectRelativeURI">
<select name="Value" id="node_template_select">
{foreach $viewmode.viewmode_list as $option}
{if and($viewmode.viewmode_infos.herited, eq($option.id,'inherit'))}
<option value="{$option.id}" selected="selected">{$option.name} (ノード{$viewmode.viewmode_infos.herited_from.node_id}から「{$viewmode.viewmode_infos.viewmode_name}」)</option>
{else}
<option value="{$option.id}"{if and(eq($viewmode.viewmode_infos.viewmode,$option.id),not($viewmode.viewmode_infos.herited))} selected="selected"{/if}>{$option.name}</option>
{/if}
{/foreach}
</select>
<input type="submit" class="button-disabled" value="更新" name="UpdateTemplate" id="node_template_button">
</form>
</td></tr>
</tbody>
</table>
</div>

{literal}
<script type="text/javascript">
(function () {
 YUI(YUI3_config).use('ezmodalwindow', function (Y) {
   Y.one('#node_template_select').on('change', function(e) {
     Y.one('#node_template_button').replaceClass('button-disabled','button')
   });
  });
})();
</script>
{/literal}

{def $child_viewmode = fetch('template', 'child_viewmode_list', hash('node_id', $node.node_id))}

<div class="block">
<table class="list" cellspacing="0">
<thead>
<tr>
<tr><th width="100%">子ノードテンプレート</th>
</tr>
</thead>
<tbody>
<tr><td>
<form action={'template/update'|ezurl} method="post">
<input type="hidden" name="Type" value="child_template">
<input type="hidden" name="NodeID" value="{$node.node_id}">
<input type="hidden" value="{$node.url_alias}" name="RedirectRelativeURI">
<select name="Value" id="child_template_select">
{foreach $child_viewmode.viewmode_list as $option}
{if and($child_viewmode.viewmode_infos.herited, eq($option.id,'inherit'))}
<option value="{$option.id}" selected="selected">{$option.name} (ノード{$child_viewmode.viewmode_infos.herited_from.node_id}から「{$child_viewmode.viewmode_infos.viewmode_name}」)</option>
{else}
<option value="{$option.id}"{if and(eq($child_viewmode.viewmode_infos.viewmode,$option.id),not($child_viewmode.viewmode_infos.herited))} selected="selected"{/if}>{$option.name}</option>
{/if}
{/foreach}
</select>
<input type="submit" class="button-disabled" value="更新" name="UpdateTemplate" id="child_template_button">
</form>
</td></tr>
</tbody>
</table>
</div>

{literal}
<script type="text/javascript">
(function () {
 YUI(YUI3_config).use('ezmodalwindow', function (Y) {
   Y.one('#child_template_select').on('change', function(e) {
     Y.one('#child_template_button').replaceClass('button-disabled','button')
   });
  });
})();
</script>
{/literal}

{def $layout = fetch('template', 'layout_list', hash('node_id', $node.node_id))}
<div class="block">
<table class="list" cellspacing="0">
<thead>
<tr>
<tr><th width="100%">ノードレイアウト</th>
</tr>
</thead>
<tbody>
<tr><td>
<form action={'template/update'|ezurl} method="post">
<input type="hidden" name="Type" value="node_layout">
<input type="hidden" name="NodeID" value="{$node.node_id}">
<input type="hidden" value="{$node.url_alias}" name="RedirectRelativeURI">
<select name="Value" id="node_layout_select">

{foreach $layout.layout_list as $option}
{if and($layout.layout_infos.herited, eq($option.id,'inherit'))}
<option value="{$option.id}" selected="selected">{$option.name} (ノード{$layout.layout_infos.herited_from.node_id}から「{$layout.layout_infos.layout_name}」)</option>
{else}
<option value="{$option.id}"{if and(eq($layout.layout_infos.layout,$option.id),not($layout.layout_infos.herited))} selected="selected"{/if}>{$option.name}</option>
{/if}
{/foreach}
</select>
<input type="submit" class="button-disabled" value="更新" name="UpdateTemplate" id="node_layout_button">
</form>
</td></tr>
</tbody>
</table>
</div>

{literal}
<script type="text/javascript">
(function () {
 YUI(YUI3_config).use('ezmodalwindow', function (Y) {
   Y.one('#node_layout_select').on('change', function(e) {
     Y.one('#node_layout_button').replaceClass('button-disabled','button')
   });
  });
})();
</script>
{/literal}
