{ezcss_require('flags.css')}

{def $container_classes = ezini('Content', 'ContainerClass', 'easycms.ini')}
{if $container_classes|contains( $node.class_identifier )}
{include uri='design:parts/flag_container.tpl'}
{else}
{include uri='design:parts/flag_content.tpl'}
{/if}
