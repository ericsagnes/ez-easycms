{def $checked = false()}
<div id="flags-user-tab">

{def $count = -1}
{foreach $node.path_array as $id => $path_node_id reverse}
{set $count = $count|inc}

{if lt($path_node_id,2)}{break}{/if}
{def $current_node = fetch('content','node', hash('node_id', $path_node_id))}
{if not($container_classes|contains( $current_node.class_identifier ))}{continue}{/if}


{def $categories = fetch('flags','content_flag_data', hash(
    'node_id', $path_node_id
))}

<div class="block">
<table class="list" cellspacing="0">
<thead>
<tr>
<tr><th width="100%">{node_view_gui content_node=$current_node view=flag_path}</th>
<th>{if gt($count,1)}<input type="submit" value="表示" class="button toggle-button" />{/if}</th>
</tr>
</thead>
<tbody{if gt($count,1)} style="display:none;"{/if}>
<tr class="bg-light">
<td colspan="2">
<form action={'flags/assign'|ezurl()} method="post">
<input type="hidden" value="{$node.contentobject_id}" name="ObjectID">
<input type="hidden" value="{$node.url_alias}" name="RedirectRelativeURI">
{def $flag_list = ''}
{def $has_flag = false()}
{foreach $categories as $category}
{if eq($category.flags|count,0)}{continue}{/if}
<fieldset>
<legend>{$category.name}</legend>
<ul class="flags-list">
{foreach $category.flags as $flag}
{set $has_flag = true()}
{if $flag.assigned_objects_id|contains($node.contentobject_id)} {set $checked=' checked="checked"'} {else} {set $checked=''} {/if}
<li><input type="checkbox"{$checked} name="Flags[{$flag.id}]" /> {$flag.name}</li>
{set $flag_list = concat($flag_list, ',', $flag.id)}
{/foreach}
</ul>
</fieldset>
{/foreach}
<div>
{if $has_flag}
<input type="hidden" value="{$flag_list}" name="FlagList">
<input type="submit" value="{'適用'|i18n( 'design/admin/node/view/full' )}" name="AssignButton" class="button" />
{else}
<p>{$current_node.name}にはフラグが設定されていません。<br /><a href={$current_node.url_alias|ezurl}>フラグを設定する</a></p>
{/if}
</div>
</form>
</td>
</tr>
</tbody>
</table>
</div>

{undef $has_flag}
{undef $flag_list}
{undef $current_node}
{undef $categories}
{/foreach}

</div>
{include uri='design:flags/toggle_button.tpl'}
