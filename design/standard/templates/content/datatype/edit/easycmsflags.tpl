{ezcss_require('flags.css')}


<h3>テンプレート</h3>
{def $viewmodes = ezini( 'Flags', 'viewmodes', 'easycms.ini' )}
<p><select name="{$attribute_base}_flag_viewmode_{$attribute.id}">
{foreach $viewmodes as $id => $value}
<option value="{$id}"{if eq($attribute.content.viewmode, $id)} selected="selected"{/if}>{$value}</option>
{/foreach}
</select></p>

<h3>表示数</h3>
<p><input type="text" value="{if $attribute.content.items_per_page|null()}10{else}{$attribute.content.items_per_page}{/if}" name="{$attribute_base}_flag_items_per_page_{$attribute.id}" /></p>

<h3>ソート方法</h3>
{def $viewmodes = ezini( 'Flags', 'sort_fields', 'easycms.ini' )}
<p><select name="{$attribute_base}_flag_sort_field_{$attribute.id}">
{foreach $viewmodes as $id => $value}
<option value="{$id}"{if eq($attribute.content.sort_field, $id)} selected="selected"{/if}>{$value}</option>
{/foreach}
</select>
<select name="{$attribute_base}_flag_sort_order_{$attribute.id}">
<option value="1"{if eq($attribute.content.sort_order, 1)} selected="selected"{/if}>昇順</option>
<option value="0"{if eq($attribute.content.sort_order, 0)} selected="selected"{/if}>降順</option>
</select></p>

<h3>日付表示方法</h3>
<p><input type="text" value="{if $attribute.content.date_format|null}yyyy/mm/dd{else}{$attribute.content.date_format}{/if}" name="{$attribute_base}_flag_date_format_{$attribute.id}" /></p>

<h3>フォルダー</h3>
{if $attribute.content.parent_node_id|null()|not}
{def $folder = fetch('content', 'node', hash('node_id', $attribute.content.parent_node_id))}
<p>選択されたフォルダー: <strong>{$folder.name}</strong> (ノードID: {$folder.node_id})</p>
{/if}
<input type="hidden" name="{$attribute_base}_browse_for_object_start_node[{$attribute.id}]" value="{$attribute.object.main_node.parent.node_id}" />
<input type="hidden" name="{$attribute_base}_data_object_relation_id_{$attribute.id}" value="0" />
<input type="hidden" name="{$attribute_base}_folder_node_id_{$attribute.id}" value="{$attribute.content.folder_node_id}">
<p><input class="button ezobject-relation-add-button" type="submit" name="CustomActionButton[{$attribute.id}_browse_object]" value="{'フォルダーの選択'|i18n( 'design/standard/content/datatype' )}" /></p>

{if $attribute.content.parent_node_id|null()|not}

{def $container_classes = ezini('Content', 'ContainerClass', 'easycms.ini')}
<h3>絞り込みフラグ</h3>
{def $checked = ""}
<div id="flags-user-tab">


{def $count = 0}
{foreach $folder.path_array as $id => $path_node_id reverse}
{set $count = $count|inc}

{if lt($path_node_id,2)}{break}{/if}
{def $current_node = fetch('content','node', hash('node_id', $path_node_id))}
{if not($container_classes|contains( $current_node.class_identifier ))}{continue}{/if}

{def $categories = fetch('flags','content_flag_data', hash(
    'node_id', $current_node.node_id
))}

{def $has_flag=false()}
<div class="block">
<table class="list" cellspacing="0">
<thead>
<tr>
<tr><th width="100%">{node_view_gui content_node=$current_node view=flag_path}</th>
<th>{if gt($count,1)}<input type="submit" value="表示" class="button toggle-button" />{/if}</th>
</tr>
</thead>
<tbody{if gt($count,1)} style="display:none;"{/if}>
<tr class="bg-light">
<td colspan="2">
{foreach $categories as $category}
{if $category.flags|count}
{set $has_flag = true()}
<fieldset>
<legend>{$category.name}</legend>
<ul class="flags-list">
{foreach $category.flags as $flag}
{if $attribute.content.flag_list|contains($flag.id)} {set $checked=' checked="checked"'} {else} {set $checked=''} {/if}
<li><input type="checkbox"{$checked} name="{$attribute_base}_flag_list_{$attribute.id}[{$flag.id}]" /> {$flag.name}</li>
{/foreach}
</ul>
</fieldset>
{/if}
{/foreach}
{if $has_flag|not}
<p>{$current_node.name}にはフラグが設定されていません。<p>
{/if}
</td>
</tr>
</tbody>
</table>
</div>
{undef $has_flag}
{/foreach}

{/if}
</div>

{include uri='design:flags/toggle_button.tpl'}
