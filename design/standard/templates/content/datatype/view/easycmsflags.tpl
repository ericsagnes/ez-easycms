{def $items_per_page = $attribute.content.items_per_page}
{def $items = ""}
{def $items_count = ""}
{def $max_depth = 8}

{def $fetch_hash = ""}
{if $parent_node_id}
{elseif $attribute.content.parent_node_id|is_null|not}
{def $parent_node_id = $attribute.content.parent_node_id}
{else}
{def $parent_node_id = $attribute.object.main_node.node_id}
{/if}

{def $content_classes = ezini('Content', 'ContentClass', 'easycms.ini')}

{if $view_parameters.flag}
{if $view_parameters.flag|contains('&')}
{def $flag_list = $view_parameters.flag|explode('&')}
{else}
{def $flag_list = $view_parameters.flag}
{/if}
{set $fetch_hash = hash(
        'id', 'FlagsAttributeFilter',
        'params', hash( 'flag_id', $flag_list )
    )}
{elseif $attribute.content.flag_list|count}
{set $fetch_hash = hash(
        'id', 'FlagsAttributeFilter',
        'params', hash( 'flag_id', $attribute.content.flag_list )
    )}
{else}
{set $fetch_hash = false()}
{/if}

{if $attribute.content.sort_field|contains('/')}
{def $sort_array = array('attribute', $attribute.content.sort_order, $attribute.content.sort_field)}
{else}
{def $sort_array = array( $attribute.content.sort_field, $attribute.content.sort_order )}
{/if}

{set $items_count = fetch('content', 'list', hash(
    'parent_node_id', $parent_node_id,
    'extended_attribute_filter', $fetch_hash,
    'depth', $max_depth,
    'class_filter_type', 'include',
    'class_filter_array', $content_classes
))}

{set $items_count = $items_count|count}
{set $items = fetch('content', 'list', hash(
    'parent_node_id', $parent_node_id,
    'sort_by', $sort_array,
    'limit', $items_per_page,
    'offset', $view_parameters.offset,
    'depth', $max_depth,
    'extended_attribute_filter', $fetch_hash,
    'class_filter_type', 'include',
    'class_filter_array', $content_classes
))}

{if is_unset($viewmode)}
{def $viewmode=$attribute.content.viewmode}
{/if}

{def $item_category_flags=false()}
{def $last_group_header = ""}
{def $group_header = false()}

{foreach $items as $item}
{set $group_header=false()}
{set $item_category_flags = fetch('flags','object_category_list', hash( 'object_id', $item.contentobject_id, 'category_id', $view_parameters.group_by ))}
{if ne($last_group_header,$item_category_flags[0].name)}
{set $last_group_header = $item_category_flags[0].name}
{set $group_header = $item_category_flags[0]}
{/if}
{node_view_gui content_node=$item view=$viewmode date_format=$attribute.content.date_format_ez group_header=$group_header}
{/foreach}

{def $base_url = $attribute.object.main_node.url_alias}
{if $view_parameters.flag}
{set $base_url = concat($base_url,'/(flag)/',$view_parameters.flag)}
{/if}
{if $view_parameters.group_by}
{set $base_url = concat($base_url,'/(group_by)/',$view_parameters.group_by)}
{/if}
{def $offset = 0}
{if $view_parameters.offset}
{set $offset = $view_parameters.offset}
{/if}

{if ne($no_pagination,true())}
{def $max_pages = div($items_count, $items_per_page)|ceil()}
{def $page_nb = div($offset|inc, $items_per_page)|ceil}

{include uri='design:easycms/pagination.tpl' page_nb=$page_nb items_per_page=$items_per_page max_pages=$max_pages base_url=$base_url}
{/if}
