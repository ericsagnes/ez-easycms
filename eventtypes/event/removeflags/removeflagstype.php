<?php

class RemoveFlagsType extends eZWorkflowEventType
{
  const WORKFLOW_TYPE_STRING = "removeflags";
  public function __construct()
  {
    parent::__construct( RemoveFlagsType::WORKFLOW_TYPE_STRING, 'オブジェクトフラグの削除' );
  }

  public function execute( $process, $event )
  {
    $parameters = $process->attribute( 'parameter_list' );
    foreach ($parameters['node_id_list'] as $node_id){
      $object_id = eZContentObjectTreeNode::fetch( $node_id )->attribute('contentobject_id');
      easycmsFlagLinkObject::deleteObject( $object_id );
      easycmsFlagObject::deleteObject( $object_id );
      easycmsFlagCategoryObject::deleteObject( $object_id );
    }
    return eZWorkflowType::STATUS_ACCEPTED;
  }
}

eZWorkflowEventType::registerEventType( RemoveFlagsType::WORKFLOW_TYPE_STRING, 'removeflagstype' );

?>

