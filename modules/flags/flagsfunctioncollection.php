<?php

/**
 * eZTagsFunctionCollection class implements fetch functions for eztags
 *
 */
class flagsFunctionCollection
{

  static public function fetchContentFlagData( $node_id )
  {
    $result = easycmsFlagCategoryObject::fetchContentFlagData( $node_id );

    if ( is_array( $result ) && !empty( $result ) )
      return array( 'result' => $result );
    else
      return array( 'result' => false );
  }

  static public function fetchContainerFlagData( $node_id )
  {
    $result = easycmsFlagCategoryObject::fetchContainerFlagData( $node_id );

    if ( is_array( $result ) && !empty( $result ) )
      return array( 'result' => $result );
    else
      return array( 'result' => false );
  }

  static public function fetchObjectFlagList( $object_id )
  {
    $result = easycmsFlagObject::fetchObjectFlagList( $object_id );

    if ( is_array( $result ) && !empty( $result ) )
      return array( 'result' => $result );
    else
      return array( 'result' => false );
  }

  static public function fetchObjectCategoryFlagList( $object_id, $category_id )
  {
    $result = easycmsFlagObject::fetchObjectCategoryFlagList( $object_id, $category_id );

    if ( is_array( $result ) && !empty( $result ) )
      return array( 'result' => $result );
    else
      return array( 'result' => false );
  }

  static public function fetchFlag( $flag_id )
  {
    $result = easycmsFlagObject::fetchFlag( $flag_id );

    if ( $result )
      return array( 'result' => $result );
    else
      return array( 'result' => false );
  }

  static public function fetchNodeCategoriesList( $node_id )
  {
    $result = easycmsFlagCategoryObject::fetchNodeCategoriesList( $node_id );

    if ( is_array( $result ) && !empty( $result ) )
      return array( 'result' => $result );
    else
      return array( 'result' => false );
  }

  static public function fetchCategoryFlagList( $object_id, $category )
  {

    $result = easycmsFlagCategoryObject::fetchObjectCategoryFlagList( $object_id, $category );

    if ( is_array( $result ) && !empty( $result ) )
      return array( 'result' => $result );
    else
      return array( 'result' => false );
  }

}

?>
