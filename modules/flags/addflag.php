<?php

$Module = $Params['Module'];
$Result = array();
$Result['content'] = '';

// Identify whether the input data was submitted through URL parameters or through POST
if ( $Module->isCurrentAction( 'AddFlag' )   and
     $Module->hasActionParameter( 'FlagName' ) and
     $Module->hasActionParameter( 'FlagObjectID' ) and
     $Module->hasActionParameter( 'Category' ) )
{
    $flag_priority = $Module->actionParameter( 'FlagPriority' );
    $flag_name = $Module->actionParameter( 'FlagName' );
    $category = $Module->actionParameter( 'Category' );
    $flag_objectid = $Module->actionParameter( 'FlagObjectID' );
}

if ( $flag_name and $category and $flag_objectid )
{
    $flag = new easycmsFlagObject( array('name'=>$flag_name, 'contentobject_id' => $flag_objectid, 'priority' => $flag_priority) );
    $flag->store();
    if ( $category != "empty" ){
        $flag_category = new easycmsFlagCategoryLinkObject( array('flag_id'=>$flag->attribute('id'), 'flag_category_id' => (int) $category ) );
        $flag_category->store();
    }
}
$Module->hasActionParameter( 'RedirectRelativeURI' ) ? $Module->redirectTo( $Module->actionParameter( 'RedirectRelativeURI' ) ) : $Module->redirectTo( '/' );

?>


