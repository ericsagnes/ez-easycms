<?php

$Module = $Params['Module'];
$Result = array();
$Result['content'] = '';

// Identify whether the input data was submitted through URL parameters or through POST
if ( $Module->isCurrentAction( 'DeleteFlag' )   and
     $Module->hasActionParameter( 'FlagID' ) )
{
    $flag_id = (int) $Module->actionParameter( 'FlagID' );
}

if ( $flag_id )
{
    $flag = eZPersistentObject::fetchObject( easycmsFlagObject::definition(), null, array('id' => $flag_id) );
    $flag->remove();
    $category_links = eZPersistentObject::fetchObjectList( easycmsFlagCategoryLinkObject::definition(), null, array('flag_id' => $flag_id) );
    foreach( $category_links as $category_link ){
        $category_link->remove();
    }

}
$Module->hasActionParameter( 'RedirectRelativeURI' ) ? $Module->redirectTo( $Module->actionParameter( 'RedirectRelativeURI' ) ) : $Module->redirectTo( '/' );

?>


