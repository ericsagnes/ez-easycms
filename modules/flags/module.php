<?php

$Module = array( 'name'            => 'flags',
                 'variable_params' => false );

$ViewList = array();
$ViewList['assign'] = array(
    'functions'               => array( 'assign' ),
    'script' => 'assign.php',
    'params' => array( 'ObjectID', 'FlagList' ),
    'single_post_actions' => array( 'AssignButton' => 'Assign' ),
    'post_action_parameters' => array( 'Assign' => array( 'ObjectID'            => 'ObjectID',
                                                          'Flags'               => 'Flags',
                                                          'FlagList'            => 'FlagList',
                                                          'RedirectRelativeURI' => 'RedirectRelativeURI' ) )
);

$ViewList['addflag'] = array(
    'functions'               => array( 'addflag' ),
    'script' => 'addflag.php',
    'params' => array( 'FlagName', 'Category', 'FlagObjectID' ),
    'single_post_actions' => array( 'AddFlagButton' => 'AddFlag' ),
    'post_action_parameters' => array( 'AddFlag' => array('FlagName'            => 'FlagName',
                                                          'Category'            => 'Category',
                                                          'FlagObjectID'        => 'FlagObjectID',
                                                          'FlagPriority'        => 'FlagPriority',
                                                          'RedirectRelativeURI' => 'RedirectRelativeURI' ) )
);

$ViewList['addcategory'] = array(
    'functions'               => array( 'addflag' ),
    'script' => 'addcategory.php',
    'params' => array( 'CategoryName', 'CategoryObjectID' ),
    'single_post_actions' => array( 'AddCategoryButton' => 'AddCategory' ),
    'post_action_parameters' => array( 'AddCategory' => array('CategoryName'            => 'CategoryName',
                                                              'CategoryObjectID'        => 'CategoryObjectID',
                                                              'CategorySystem'          => 'CategorySystem',
                                                              'RedirectRelativeURI'     => 'RedirectRelativeURI' ) )
);

$ViewList['deleteflag'] = array(
    'functions'               => array( 'addflag' ),
    'script' => 'deleteflag.php',
    'params' => array( 'FlagID' ),
    'single_post_actions' => array( 'DeleteFlagButton' => 'DeleteFlag' ),
    'post_action_parameters' => array( 'DeleteFlag' => array('FlagID'            => 'FlagID',
                                                          'RedirectRelativeURI' => 'RedirectRelativeURI' ) )
);

$ViewList['updateflag'] = array(
    'functions'               => array( 'addflag' ),
    'script' => 'updateflag.php',
    'params' => array( 'FlagID', 'FlagName', 'Category' ),
    'single_post_actions' => array( 'UpdateFlagButton' => 'UpdateFlag' ),
    'post_action_parameters' => array( 'UpdateFlag' => array('FlagName'            => 'FlagName',
                                                          'FlagID'            => 'FlagID',
                                                          'FlagPriority'        => 'FlagPriority',
                                                          'Category'            => 'Category',
                                                          'RedirectRelativeURI' => 'RedirectRelativeURI' ) )
);

$ViewList['deletecategory'] = array(
    'functions'               => array( 'addflag' ),
    'script' => 'deletecategory.php',
    'params' => array( 'CategoryID' ),
    'single_post_actions' => array( 'DeleteCategoryButton' => 'DeleteCategory' ),
    'post_action_parameters' => array( 'DeleteCategory' => array('CategoryID'            => 'CategoryID',
                                                          'RedirectRelativeURI' => 'RedirectRelativeURI' ) )
);

$ViewList['updatecategory'] = array(
    'functions'               => array( 'addflag' ),
    'script' => 'updatecategory.php',
    'params' => array( 'CategoryID', 'CategoryName' ),
    'single_post_actions' => array( 'UpdateCategoryButton' => 'UpdateCategory' ),
    'post_action_parameters' => array( 'UpdateCategory' => array('CategoryID'            => 'CategoryID',
                                                          'CategoryName'            => 'CategoryName',
                                                          'CategorySystem'            => 'CategorySystem',
                                                          'RedirectRelativeURI' => 'RedirectRelativeURI' ) )
);

$ViewList['dashboard'] = array(
    'functions'               => array( 'dashboard' ),
    'script'                  => 'dashboard.php',
    'default_navigation_part' => 'eztagsnavigationpart',
    'params'                  => array(),
    'unordered_params'        => array( 'offset' => 'Offset' ) );


$FunctionList = array();
$FunctionList['dashboard']     = array();
$FunctionList['assign']        = array();
$FunctionList['addflag']        = array();

?>
