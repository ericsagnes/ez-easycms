<?php

$Module = $Params['Module'];
$Result = array();
$Result['content'] = '';

var_dump('add category');

// Identify whether the input data was submitted through URL parameters or through POST
if ( $Module->isCurrentAction( 'AddCategory' )   and
     $Module->hasActionParameter( 'CategoryObjectID' ) and
     $Module->hasActionParameter( 'CategoryName' ) )
{
    $category_name = $Module->actionParameter( 'CategoryName' );
    $category_system = $Module->hasActionParameter( 'CategorySystem' ) ? 1 : 0;
    $category_objectid = $Module->actionParameter( 'CategoryObjectID' );
}

if ( $category_name and $category_objectid )
{
    $category = new easycmsFlagCategoryObject( array('name'=>$category_name, 'contentobject_id'=>$category_objectid, 'system_category'=>$category_system) );
    $category->store();
}
$Module->hasActionParameter( 'RedirectRelativeURI' ) ? $Module->redirectTo( $Module->actionParameter( 'RedirectRelativeURI' ) ) : $Module->redirectTo( '/' );

?>


