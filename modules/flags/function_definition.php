<?php

$FunctionList = array();


$FunctionList['content_flag_data'] = array( 'name'  => 'container_flag_data',
                               'operation_types' => array( 'read' ),
                               'call_method'     => array( 'class'  => 'flagsFunctionCollection',
                                                           'method' => 'fetchContentFlagData' ),
                              'parameter_type'  => 'standard',
                              'parameters'      => array( array( 'name'     => 'node_id',
                                                                 'type'     => 'integer',
                                                                 'required' => true ) ) );

$FunctionList['container_flag_data'] = array( 'name'  => 'container_flag_data',
                               'operation_types' => array( 'read' ),
                               'call_method'     => array( 'class'  => 'flagsFunctionCollection',
                                                           'method' => 'fetchContainerFlagData' ),
                              'parameter_type'  => 'standard',
                              'parameters'      => array( array( 'name'     => 'node_id',
                                                                 'type'     => 'integer',
                                                                 'required' => true ) ) );

$FunctionList['categories'] = array( 'name'            => 'categories',
                                'operation_types' => array( 'read' ),
                                'call_method'     => array( 'class'  => 'flagsFunctionCollection',
                                                            'method' => 'fetchNodeCategoriesList' ),
                                'parameter_type'  => 'standard',
                                'parameters'      => array( array( 'name'     => 'node_id',
                                                                   'type'     => 'integer',
                                                                   'required' => false ) ) );

$FunctionList['list'] = array( 'name'            => 'list',
                              'operation_types' => array( 'read' ),
                              'call_method'     => array( 'class'  => 'flagsFunctionCollection',
                                                          'method' => 'fetchObjectFlagList' ),
                              'parameter_type'  => 'standard',
                              'parameters'      => array( array( 'name'     => 'object_id',
                                                                  'type'     => 'integer',
                                                                  'required' => true ) ) );

$FunctionList['flag'] = array( 'name'            => 'flag',
                              'operation_types' => array( 'read' ),
                              'call_method'     => array( 'class'  => 'flagsFunctionCollection',
                                                          'method' => 'fetchFlag' ),
                              'parameter_type'  => 'standard',
                              'parameters'      => array( array( 'name'     => 'flag_id',
                                                                  'type'     => 'integer',
                                                                  'required' => true )
                                                                ));

$FunctionList['object_category_list'] = array( 'name'            => 'object_category_list',
                              'operation_types' => array( 'read' ),
                              'call_method'     => array( 'class'  => 'flagsFunctionCollection',
                                                          'method' => 'fetchObjectCategoryFlagList' ),
                              'parameter_type'  => 'standard',
                              'parameters'      => array( array( 'name'     => 'object_id',
                                                                  'type'     => 'integer',
                                                                  'required' => true ),
                                                          array( 'name'     => 'category_id',
                                                                  'type'     => 'integer',
                                                                  'required' => true )
                                                                ));

$FunctionList['category_flags'] = array( 'name'            => 'category_flags',
                              'operation_types' => array( 'read' ),
                              'call_method'     => array( 'class'  => 'flagsFunctionCollection',
                                                          'method' => 'fetchCategoryFlagList' ),
                              'parameter_type'  => 'standard',
                              'parameters'      => array( array( 'name'     => 'object_id',
                                                                  'type'     => 'integer',
                                                                  'required' => true ),
                                                         array(  'name'     => 'category',
                                                                  'type'     => 'string',
                                                                  'required' => true ) ) );

?>
