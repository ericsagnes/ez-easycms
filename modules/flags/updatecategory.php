<?php

$Module = $Params['Module'];
$Result = array();
$Result['content'] = '';

var_dump('update category');

// Identify whether the input data was submitted through URL parameters or through POST
if ( $Module->isCurrentAction( 'UpdateCategory' )   and
     $Module->hasActionParameter( 'CategoryName' ) and
     $Module->hasActionParameter( 'CategoryID' ) )
{
    $category_id = (int) $Module->actionParameter( 'CategoryID' );
    $category_system = $Module->hasActionParameter( 'CategorySystem' ) ? 1 : 0;
    $category_name = $Module->actionParameter( 'CategoryName' );
}

if ( $category_name and $category_id )
{
    $category = eZPersistentObject::fetchObject( easycmsFlagCategoryObject::definition(), null, array('id' => $category_id) );
    $category->setAttribute( 'name', $category_name );
    $category->setAttribute( 'system_category', $category_system );
    $category->store();
}
$Module->hasActionParameter( 'RedirectRelativeURI' ) ? $Module->redirectTo( rawurlencode( $Module->actionParameter( 'RedirectRelativeURI' ) ) ) : $Module->redirectTo( '/' );

?>
