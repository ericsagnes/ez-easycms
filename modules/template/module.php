<?php

$Module = array( 'name'            => 'template',
                 'variable_params' => false );

$ViewList = array();
$ViewList['update'] = array(
    'functions'               => array( 'update' ),
    'script' => 'update.php',
    'params' => array( 'NodeID', 'Type', 'Value' ),
    'single_post_actions' => array( 'UpdateTemplate' => 'Update' ),
    'post_action_parameters' => array( 'Update' => array( 'NodeID'              => 'NodeID',
                                                          'Type'                => 'Type',
                                                          'Value'               => 'Value',
                                                          'RedirectRelativeURI' => 'RedirectRelativeURI' ) )
);

$FunctionList = array();
$FunctionList['update']     = array();

?>
