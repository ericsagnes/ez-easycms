<?php

$Module = $Params['Module'];
$Result = array();
$Result['content'] = '';

if ( $Module->isCurrentAction( 'Update' )   and
     $Module->hasActionParameter( 'NodeID' ) and
     $Module->hasActionParameter( 'Value' ) and
     $Module->hasActionParameter( 'Type' ) )
{
    $node_id = (int) $Module->actionParameter( 'NodeID' );
    $type = $Module->actionParameter( 'Type' );
    $value = $Module->actionParameter( 'Value' );
}

var_dump($type);
var_dump($node_id);
var_dump($value);

if ( $value and $type and $node_id )
{
  if( $type == "node_template" ){
    if( $link = eZPersistentObject::fetchObject( easycmsViewModeLinkObject::definition(), null, array('node_id' => $node_id) ) ){
      $link->setAttribute( 'view', $value );
    }
    else{
      $link = new easycmsViewModeLinkObject( array('node_id'=>$node_id, 'view'=>$value) );
    }
    $link->store();
  } elseif ( $type == "child_template" ){
    if( $link = eZPersistentObject::fetchObject( easycmsChildViewModeLinkObject::definition(), null, array('node_id' => $node_id) ) ){
      $link->setAttribute( 'view', $value );
    }
    else{
      $link = new easycmsChildViewModeLinkObject( array('node_id'=>$node_id, 'view'=>$value) );
    }
    $link->store();
  } elseif ( $type == "node_layout" ){
    if( $link = eZPersistentObject::fetchObject( easycmsLayoutLinkObject::definition(), null, array('node_id' => $node_id) ) ){
      $link->setAttribute( 'layout', $value );
    }
    else{
      $link = new easycmsLayoutLinkObject( array('node_id'=>$node_id, 'layout'=>$value) );
    }
    $link->store();
  }
}
$Module->hasActionParameter( 'RedirectRelativeURI' ) ? $Module->redirectTo( rawurlencode( $Module->actionParameter( 'RedirectRelativeURI' ) ) ) : $Module->redirectTo( '/' );

?>
