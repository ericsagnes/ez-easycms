<?php

$FunctionList = array();


$FunctionList['viewmode'] = array( 'name'  => 'viewmode',
                               'operation_types' => array( 'read' ),
                               'call_method'     => array( 'class'  => 'templateFunctionCollection',
                                                           'method' => 'fetchViewMode' ),
                              'parameter_type'  => 'standard',
                              'parameters'      => array( array( 'name'     => 'node_id',
                                                                 'type'     => 'integer',
                                                                 'required' => true ) ) );

$FunctionList['layout'] = array( 'name'  => 'layout',
                               'operation_types' => array( 'read' ),
                               'call_method'     => array( 'class'  => 'templateFunctionCollection',
                                                           'method' => 'fetchLayout' ),
                              'parameter_type'  => 'standard',
                              'parameters'      => array( array( 'name'     => 'node_id',
                                                                 'type'     => 'integer',
                                                                 'required' => false ) ) );

$FunctionList['viewmode_list'] = array( 'name'  => 'viewmode_list',
                               'operation_types' => array( 'read' ),
                               'call_method'     => array( 'class'  => 'templateFunctionCollection',
                                                           'method' => 'fetchViewModeList' ),
                              'parameter_type'  => 'standard',
                              'parameters'      => array( array( 'name'     => 'node_id',
                                                                 'type'     => 'integer',
                                                                 'required' => true ) ) );

$FunctionList['child_viewmode_list'] = array( 'name'  => 'child_viewmode_list',
                               'operation_types' => array( 'read' ),
                               'call_method'     => array( 'class'  => 'templateFunctionCollection',
                                                           'method' => 'fetchChildViewModeList' ),
                              'parameter_type'  => 'standard',
                              'parameters'      => array( array( 'name'     => 'node_id',
                                                                 'type'     => 'integer',
                                                                 'required' => true ) ) );
$FunctionList['layout_list'] = array( 'name'  => 'child_template_list',
                               'operation_types' => array( 'read' ),
                               'call_method'     => array( 'class'  => 'templateFunctionCollection',
                                                           'method' => 'fetchLayoutList' ),
                              'parameter_type'  => 'standard',
                              'parameters'      => array( array( 'name'     => 'node_id',
                                                                 'type'     => 'integer',
                                                                 'required' => true ) ) );


?>
