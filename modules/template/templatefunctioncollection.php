<?php

class templateFunctionCollection
{

  static public function fetchLayout( $node_id )
  {
    $layout_info = easycmsLayoutLinkObject::layout( $node_id );
    $layout = $layout_info['layout'];
    return array( 'result' => $layout );
  }

  static public function fetchLayoutList( $node_id )
  {
    $layout_list_settings = eZINI::instance('easycms.ini')->variable('Templates', 'layouts');
    $layout_list = array();
    $layout_infos = easycmsLayoutLinkObject::layout( $node_id );
    foreach($layout_list_settings as $id => $name){
        $layout_list[] = array('id' => $id, 'name' => $name );
    }
    return array( 'result' => 
        array(
            'layout_list' => $layout_list,
            'layout_infos' => $layout_infos
        )
    );
  }

  static public function fetchViewMode( $node_id )
  {
    $viewmode_info = easycmsViewModeLinkObject::viewmode( $node_id );
    $viewmode = $viewmode_info['viewmode'];
    return array( 'result' => $viewmode );
  }

  static public function fetchViewModeList( $node_id )
  {
    $viewmode_list_settings = eZINI::instance('easycms.ini')->variable('Templates', 'viewmodes');
    $viewmode_list = array();
    $viewmode_infos = easycmsViewModeLinkObject::viewmode( $node_id );
    foreach($viewmode_list_settings as $id => $name){
        $viewmode_list[] = array('id' => $id, 'name' => $name );
    }
    return array( 'result' => 
        array(
            'viewmode_list' => $viewmode_list,
            'viewmode_infos' => $viewmode_infos
        )
    );
  }

  static public function fetchChildViewMode( $node_id )
  {
    $viewmode_info = easycmsChildViewModeLinkObject::viewmode( $node_id );
    $viewmode = $viewmode_info['viewmode'];
    return array( 'result' => $viewmode );
  }

  static public function fetchChildViewModeList( $node_id )
  {
    $viewmode_list_settings = eZINI::instance('easycms.ini')->variable('Templates', 'viewmodes');
    $viewmode_list = array();
    $viewmode_infos = easycmsChildViewModeLinkObject::viewmode( $node_id );
    foreach($viewmode_list_settings as $id => $name){
        $viewmode_list[] = array('id' => $id, 'name' => $name );
    }
    return array( 'result' => 
        array(
            'viewmode_list' => $viewmode_list,
            'viewmode_infos' => $viewmode_infos
        )
    );
  }

}

?>
